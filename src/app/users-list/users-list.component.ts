import { Component } from '@angular/core';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent {
  newUserName=''
  users = [
    {
      id:'1',
      name:'Jack',
      age:21
    },
    {
      id:'2',
      name:'John',
      age:25
    },
    {
      id:'3',
      name:'Sam',
      age:29
    },
  ]

  removeUser(id:string):void{
    this.users=this.users.filter(user=>user.id !==id)
    console.log('removeUser',id)    
  }
  result=''
  setNewUserName(event: any):void{
    // console.log('setNewUserName',userName)
    this.result=(<HTMLInputElement>event.target).value;
    console.log('setNewUserName',this.result)
    this.newUserName=this.result
  }
  addUser():void{
    const uniqueId = Math.random().toString(16)
    const newUser = {
      id:uniqueId,
      name:this.newUserName,
      age:30 
    }
    this.users.push(newUser)
    this.newUserName=''
    console.log('addUser',this.result)
  }



}
